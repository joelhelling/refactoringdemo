/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package movieAfter;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author joel.helling904
 */
public class RegularPriceTest {
    
    public RegularPriceTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     * Test of getPriceCode method, of class RegularPrice.
     */
    @Test
    public void testGetPriceCode() {
        System.out.println("getPriceCode");
        RegularPrice instance = new RegularPrice();
        int expResult = 0;
        int result = instance.getPriceCode();
        assertEquals(expResult, result);
    }

    /**
     * Test of getCharge method, of class RegularPrice.
     */
    @Test
    public void testGetCharge() {
        System.out.println("getCharge");
        int daysRented = 3;
        RegularPrice instance = new RegularPrice();
        double expResult = 3.5;
        double result = instance.getCharge(daysRented);
        assertEquals(expResult, result, 0.0);
    }

    /**
     * Test of getFrequentRenterPoints method, of class RegularPrice.
     */
    @Test
    public void testGetFrequentRenterPoints() {
        System.out.println("getFrequentRenterPoints");
        int daysRented = 3;
        RegularPrice instance = new RegularPrice();
        int expResult = 1;
        int result = instance.getFrequentRenterPoints(daysRented);
        assertEquals(expResult, result);
    }
}