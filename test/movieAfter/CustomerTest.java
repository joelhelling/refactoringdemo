/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package movieAfter;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author joel.helling904
 */
public class CustomerTest {
    
    public CustomerTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     * Test of addRental method, of class Customer.
     */
    @Test
    public void testAddRental() {
        System.out.println("addRental");
        Rental arg = new Rental(new Movie("Casablanca",0),5);
        Customer instance = new Customer("John");
        instance.addRental(arg);
    }

    /**
     * Test of getName method, of class Customer.
     */
    @Test
    public void testGetName() {
        System.out.println("getName");
        Customer instance = new Customer("John");
        String expResult = "John";
        String result = instance.getName();
        assertEquals(expResult, result);
    }

    /**
     * Test of statement method, of class Customer.
     */
    @Test
    public void testStatement() {
        System.out.println("statement");
        Movie extraspecial = new Movie("Casablanca",1);
        Movie easy= new Movie("Sinbad",0);
        Movie mtime= new Movie("Peter Pan",2);
        Movie latenight= new Movie("Saw",0);
        Rental r1 = new Rental(extraspecial,3);
        Rental r2 = new Rental(easy,3);
        Rental r3 = new Rental(mtime,3);
        Rental r4 = new Rental(latenight,3);
        Customer instance = new Customer("Romeo");
        
        instance.addRental(r4);
        instance.addRental(r3);
        instance.addRental(r2);
        instance.addRental(r1);
        
        String expResult = instance.statement();
        String result = instance.statement();
        assertEquals(expResult, result);
    }
}