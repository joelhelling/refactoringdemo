/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package movieAfter;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author joel.helling904
 */
public class PriceTest {
    
    public PriceTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     * Test of getCharge method, of class Price.
     */
    @Test
    public void testGetCharge() {
        System.out.println("getCharge");
        int daysRented = 5;
        Price instance = new ChildrensPrice();
        double expResult = 4.5;
        double result = instance.getCharge(daysRented);
        assertEquals(expResult, result, 0.0);
    }

    /**
     * Test of getFrequentRenterPoints method, of class Price.
     */
    @Test
    public void testGetFrequentRenterPoints() {
        System.out.println("getFrequentRenterPoints");
        int daysRented = 7;
        Price instance = new ChildrensPrice();
        int expResult = 1;
        int result = instance.getFrequentRenterPoints(daysRented);
        assertEquals(expResult, result);

    }

    /**
     * Test of getPriceCode method, of class Price.
     */
    @Test
    public void testGetPriceCode() {
        System.out.println("getPriceCode");
        Price instance = new ChildrensPrice();
        int expResult = 2;
        int result = instance.getPriceCode();
        assertEquals(expResult, result);
    }

}