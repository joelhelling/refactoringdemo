/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package movieAfter;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author joel.helling904
 */
public class MovieTest {
    
    public MovieTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     * Test of getPriceCode method, of class Movie.
     */
    @Test
    public void testGetPriceCode() {
        System.out.println("getPriceCode");
        Movie instance = new Movie("Casablanca",0);
        int expResult = 0;
        int result = instance.getPriceCode();
        assertEquals(expResult, result);
    }

    /**
     * Test of getTitle method, of class Movie.
     */
    @Test
    public void testGetTitle() {
        System.out.println("getTitle");
        Movie instance = new Movie("Casablanca",0);
        String expResult = "Casablanca";
        String result = instance.getTitle();
        assertEquals(expResult, result);
    }

    /**
     * Test of getCharge method, of class Movie.
     */
    @Test
    public void testGetCharge() {
        System.out.println("getCharge");
        int daysRented = 3;
        Movie instance = new Movie("Casablanca",0);
        double expResult = 3.5;
        double result = instance.getCharge(daysRented);
        assertEquals(expResult, result, 0.0);
    }

    /**
     * Test of getFrequentRenterPoints method, of class Movie.
     */
    @Test
    public void testGetFrequentRenterPoints() {
        System.out.println("getFrequentRenterPoints");
        int daysRented = 10;
        Movie instance = new Movie("Casablanca",0);
        int expResult = 1;
        int result = instance.getFrequentRenterPoints(daysRented);
        assertEquals(expResult, result);

    }
}