/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package movieAfter;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author joel.helling904
 */
public class RentalTest {
    
    public RentalTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     * Test of getDaysRented method, of class Rental.
     */
    @Test
    public void testGetDaysRented() {
        System.out.println("getDaysRented");
        Rental instance = new Rental(new Movie("Casablanca",0),4);
        int expResult = 4;
        int result = instance.getDaysRented();
        assertEquals(expResult, result);
    }

    /**
     * Test of getMovie method, of class Rental.
     */
    @Test
    public void testGetMovie() {
        System.out.println("getMovie");
        Rental instance = new Rental(new Movie("Casablanca",0),3);
        Movie expResult = new Movie("Casablanca",0);
        Movie result = instance.getMovie();
        assertEquals(expResult.getTitle(), result.getTitle());
        assertEquals(expResult.getPriceCode(),result.getPriceCode());

    }

    /**
     * Test of getCharge method, of class Rental.
     */
    @Test
    public void testGetCharge() {
        System.out.println("getCharge");
        Rental instance = new Rental(new Movie("Casablanca",0),3);
        double expResult = 3.5;
        double result = instance.getCharge();
        assertEquals(expResult, result, 0.0);
    }

    /**
     * Test of getFrequentRenterPoints method, of class Rental.
     */
    @Test
    public void testGetFrequentRenterPoints() {
        System.out.println("getFrequentRenterPoints");
        Rental instance = new Rental(new Movie("Casablanca",0),3);
        int expResult = 1;
        int result = instance.getFrequentRenterPoints();
        assertEquals(expResult, result);
    }
}