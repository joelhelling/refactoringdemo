/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package movieBefore;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author joel.helling904
 */
public class RentalTest {
    
    public RentalTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     * Test of getDaysRented method, of class Rental.
     */
    @Test
    public void testGetDaysRented() {
        System.out.println("getDaysRented");
        Rental instance = new Rental(new Movie("Casablanca",0),7);
        int expResult = 7;
        int result = instance.getDaysRented();
        assertEquals(expResult, result);
    }

    /**
     * Test of getMovie method, of class Rental.
     */
    @Test
    public void testGetMovie() {
        System.out.println("getMovie");
        Rental instance = new Rental(new Movie("Casablanca",0),7);
        Movie expResult = new Movie("Casablanca",0);
        Movie result = instance.getMovie();
        assertEquals(expResult.getTitle(), result.getTitle());
        assertEquals(expResult.getPriceCode(),result.getPriceCode());

    }
}