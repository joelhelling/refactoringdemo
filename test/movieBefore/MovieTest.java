/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package movieBefore;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author joel.helling904
 */
public class MovieTest {
    
    public MovieTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     * Test of getPriceCode method, of class Movie.
     */
    @Test
    public void testGetPriceCode() {
        System.out.println("getPriceCode");
        Movie instance = new Movie("Casablanca",0);
        int expResult = 0;
        int result = instance.getPriceCode();
        assertEquals(expResult, result);

    }

    /**
     * Test of setPriceCode method, of class Movie.
     */
    @Test
    public void testSetPriceCode() {
        System.out.println("setPriceCode");
        int arg = 2;
        Movie instance = new Movie("Casablanca",0);
        instance.setPriceCode(arg);
        assertEquals(2, instance.getPriceCode());
    }

    /**
     * Test of getTitle method, of class Movie.
     */
    @Test
    public void testGetTitle() {
        System.out.println("getTitle");
        Movie instance = new Movie("Casablanca",0);
        String expResult = "Casablanca";
        String result = instance.getTitle();
        assertEquals(expResult, result);
    }
}