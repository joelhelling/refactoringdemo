/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package movieBefore;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author joel.helling904
 */
public class CustomerTest {
    
    public CustomerTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     * Test of addRental method, of class Customer.
     */
    @Test
    public void testAddRental() {
        System.out.println("addRental");
        Rental arg = new Rental(new Movie("Casablanca",0),7);
        Customer instance = new Customer("John");
        instance.addRental(arg);
        
    }

    /**
     * Test of getName method, of class Customer.
     */
    @Test
    public void testGetName() {
        System.out.println("getName");
        Customer instance = new Customer("John");
        String expResult = "John";
        String result = instance.getName();
        assertEquals(expResult, result);

    }

    /**
     * Test of statement method, of class Customer.
     */
    @Test
    public void testStatement() {
        System.out.println("statement");
        Customer instance = new Customer("John");
        Customer otherInstance = new Customer("John");
        String expResult = otherInstance.statement();
        String result = instance.statement();
        assertEquals(expResult, result);
    }
}