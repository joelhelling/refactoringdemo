/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package superclassextraction;

import java.util.ArrayList;

/**
 *
 * @author joel.helling904
 */
public abstract class Analyzer {
    static final boolean CORRECT = true;
    static final boolean INCORRECT = false;
    ArrayList<String> data;

    public abstract String analyzeData(ArrayList<String> data, int offset);

    protected void fetchData() {
        //code
    }

    public boolean parseData() {
        return CORRECT;
    }
    
    public void clearData(){
        data.clear();
    }
}
