package interfaceextraction;

import java.util.ArrayList;

public class DataAnalyzer implements IDatabaseAccess {

    ArrayList<String> data;
    static final boolean CORRECT = true;
    static final boolean INCORRECT = false;

    @Override
    public void fetchData() {
        //code
    }

    void saveData() {
    }

    @Override
    public boolean parseData() {
        return CORRECT;
    }

    @Override
    public String analyzeData(ArrayList<String> data, int offset) {
        //code
        return "";
    }
}
