/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package interfaceextraction;

import java.util.ArrayList;

/**
 *
 * @author joel.helling904
 */
public interface IDataAnalyzer {

    String analyzeData(ArrayList<String> data, int offset);

    boolean parseData();
    
}
