/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package movieAfter;

/**
 *
 * @author joel.helling904
 */
public class RegularPrice implements Price{

    @Override
    public int getPriceCode()
    {
        return 0;
    }

    @Override
    public double getCharge(int daysRented)
    {
        double amount = 2.0;
        if(daysRented > 2)
            amount += (daysRented - 2) * 1.5;
        return amount;
    }
    
    @Override
    public int getFrequentRenterPoints(int daysRented)
    {
        return 1;
    }
}

