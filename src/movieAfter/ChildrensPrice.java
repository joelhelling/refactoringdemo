/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package movieAfter;

/**
 *
 * @author joel.helling904
 */
public class ChildrensPrice implements Price{

    @Override
    public int getPriceCode()
    {
        return 2;
    }
    
    @Override
    public double getCharge(int daysRented)
    {
        double amount = 1.5;
        if(daysRented > 3)
            amount += (daysRented -3) * 1.5;
        return amount;
    }
    
    @Override
    public int getFrequentRenterPoints(int daysRented)
    {
        return 1;
    }
}
