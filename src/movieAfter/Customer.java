package movieAfter;

import java.util.Enumeration;
import java.util.Vector;

public class Customer {

    private String name;
    private Vector rentals = new Vector();

    public Customer(String name) {
        this.name = name;
    }

    public void addRental(Rental arg) {
        this.rentals.addElement(arg);
    }

    public String getName() {
        return this.name;
    }

    public String statement() {
        Enumeration retalEnum = this.rentals.elements();
        String result = "Rental Record for " + getName() + "\n";

        while (retalEnum.hasMoreElements()) {

            Rental each = (Rental) retalEnum.nextElement();

            // show figures for this rental
            result += "\t" + each.getMovie().getTitle()
                    + "\t" + each.getCharge() + "\n";
        }

        // add footer lines
        result += "Amount owed is " + this.getTotalCharge() + "\n";
        result += "You earned " + this.getFrequentRenterPoints()
                + " frequent renter points";
        return result;
    }
    
    private double getTotalCharge()
    {
        double totalAmount = 0;
        
        Enumeration retalEnum = this.rentals.elements();

        while (retalEnum.hasMoreElements()) {
            Rental each = (Rental) retalEnum.nextElement();

            totalAmount += each.getCharge();
        }
        
        return totalAmount;
    }
    
    private int getFrequentRenterPoints()
    {
        int frequentRenterPoints = 0;
        Enumeration retalEnum = this.rentals.elements();

        while (retalEnum.hasMoreElements()) {


            Rental each = (Rental) retalEnum.nextElement();

            frequentRenterPoints += each.getFrequentRenterPoints();
        }

        return frequentRenterPoints;
    }
}