/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package movieAfter;

/**
 *
 * @author joel.helling904
 */
public class NewReleasePrice implements Price{

    @Override
    public int getPriceCode()
    {
        return 1;
    }

    @Override
    public double getCharge(int daysRented)
    {
        return daysRented * 3;
    }
    
    @Override
    public int getFrequentRenterPoints(int daysRented)
    {
        if(daysRented > 1)
            return 2;
        return 1;
    }
}
