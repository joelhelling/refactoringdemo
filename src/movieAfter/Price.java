/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package movieAfter;

/**
 *
 * @author joel.helling904
 */
public interface Price {

    double getCharge(int daysRented);

    int getFrequentRenterPoints(int daysRented);

    int getPriceCode();
    
}
