/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package movieBefore;

/**
 *
 * @author joel.helling904
 */
public class CustumerTesting {
    public static void main(String[] args) {
        Movie extraspecial = new Movie("Casablanca",1);
        Movie easy= new Movie("Sinbad",0);
        Movie mtime= new Movie("Peter Pan",2);
        Movie latenight= new Movie("Saw",0);
        Rental r1 = new Rental(extraspecial,3);
        Rental r2 = new Rental(easy,3);
        Rental r3 = new Rental(mtime,3);
        Rental r4 = new Rental(latenight,3);
        Customer romeo = new Customer("Romeo");
        
        romeo.addRental(r4);
        romeo.addRental(r3);
        romeo.addRental(r2);
        romeo.addRental(r1);
        
        romeo.statement();
    }
}
